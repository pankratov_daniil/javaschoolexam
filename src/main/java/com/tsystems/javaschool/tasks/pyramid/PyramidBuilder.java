package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if(inputNumbers == null || inputNumbers.contains(null) || inputNumbers.isEmpty())
            throw new CannotBuildPyramidException();

        // there is formula, that we can use for find levels of pyramid:
        // (n*(n+1))/2 = inputNumber.length; where n - levels of pyramid
        double discriminantSqrt = Math.sqrt(1 + 4 * 2 * inputNumbers.size());

        // if there is remainder of the division, throw exception
        if(discriminantSqrt % 1 != 0)
            throw new CannotBuildPyramidException();

        // sort input numbers ascending
        Collections.sort(inputNumbers);

        int pyramidLevels = ((int)discriminantSqrt - 1)/2;

        // output pyramid
        int[][] pyramid = new int[pyramidLevels][pyramidLevels * 2 - 1];

        // pointer for inputNumbers
        int counter = 0;
        for(int i = 0; i < pyramidLevels; i++){
            // index - starting index for numbers; times - how many numbers in row
            for(int index = pyramidLevels - 1 - i, times = i + 1; times > 0; times--, index += 2)
                pyramid[i][index] = inputNumbers.get(counter++);
        }

        return pyramid;
    }
}
