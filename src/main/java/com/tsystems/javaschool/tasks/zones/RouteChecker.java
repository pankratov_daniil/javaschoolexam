package com.tsystems.javaschool.tasks.zones;

import java.util.List;
import java.util.LinkedList;

public class RouteChecker {


    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){
        for(int j = 0; j < requestedZoneIds.size(); j++) {
            // wereHere - list of zones, that we visited
            List<Integer> wereHere = new LinkedList<>();
            wereHere.add(requestedZoneIds.get(j));

            // curZone - zone, where we're now
            Zone curZone = findZoneWithId(zoneState, requestedZoneIds.get(j));
            for (int i = 0; i < requestedZoneIds.size(); i++) {
                int tempId = requestedZoneIds.get(i);
                // if we didn't here
                if (!wereHere.contains(tempId)) {
                    // find new zone
                    Zone tempZone = findZoneWithId(zoneState, tempId);
                    // if curZone and tempZone are neighbours
                    if (((curZone.getNeighbours() != null && curZone.getNeighbours().contains(tempId)) ||
                            (tempZone.getNeighbours() != null && tempZone.getNeighbours().contains(curZone.getId())))) {
                        // add new id to list of visited zones
                        wereHere.add(tempId);

                        // if we have visited all zones - there is path, so we must return true
                        if(wereHere.size() == requestedZoneIds.size())
                            return true;

                        // remember new zone as current
                        curZone = tempZone;
                        i = -1;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Find zone by it's id in list of zones
     * @param zones list of all available zones
     * @param zoneId id of zone, that we need to find
     * @return found zone if it is in zones, or null otherwise
     */
    private Zone findZoneWithId(List<Zone> zones, int zoneId){
        for (Zone checkZone : zones)
            if(checkZone.getId() == zoneId)
                return checkZone;
        return null;
    }

}
